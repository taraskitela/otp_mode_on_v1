﻿using NAND_Prog;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTP_Mode_On_v1
{
    //Ця DLL експортує операцію SetOTP Mode для перреведення чіпа  MT29F1G08ABADAWP в в цей режим 


    [Export(typeof(Operation)),
        ExportMetadata("Name", "OTP_Mode_On_v1")]

    public class OTP_Mode_On : AbstractSetFeature
    {
        public OTP_Mode_On()
        {
            name = "Enable OTP operation mode";
            based = typeof(Register);

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції Enable OTP operation mode

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0xEF;                       //  SET FEATURES (EFh)
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteAddress();
            chainInstructions.Add(instruction);

            instruction.numberOfcycles = 1;                                    ////По дефолту 1  
           // (instruction as WriteAddress).address = 0x90;                      //задаю статичну адресу . Не треба вказувати -в методі GetAddressMG  адреса буде братись з client-а 
            //instruction.numberOfcycles = (Chip.memOrg.colAdrCycles + Chip.memOrg.rowAdrCycles); // /*(5Cycle)*/ Адрес буде підставляти кожна сторінка свій
            (instruction as WriteAddress).Implementation = GetAddressMG;
            //--------------------------------------------------------------

            //--------------------------------------------------------------

            instruction = new WriteData();
            chainInstructions.Add(instruction);
            (instruction as WriteData).Implementation = GetDataMG;

            //--------------------------------------------------------------
        }
        private BufMG GetDataMG(ChipPart client)                        //Реалізація _implementation для  WriteData : Instruction
        {
            DataMG meneger = new DataMG();
            meneger.direction = Direction.Out;     //дані на вихід

            meneger.numberOfCycles = 0x04;   // write 01h to P1, followed by three cycles of 00h to P2 - P4

            byte[] data = new byte[4] { 0x01, 0x00, 0x00, 0x00 };
            meneger.SetDataPlace(data);

            return meneger;

        }

        protected override  BufMG GetAddressMG(ulong addr, int size, ChipPart client)    //Реалізація _implementation для  WriteAddress : Instruction .Дійсна для Page .Для інших сутностей треба перевизначати
        {
            //_numberOfcycles = instruction.NumberOfCycles;
            //    if (instruction.Value != null)                //Тобто в інструкції чітко зашита постійна адреса (наприклад адреса ID регістра) то її і використовую 
            //        _address = (ulong)instruction.Value;
            //    else
            //        _address =  owner.GetObjectAdrress();     //в іншому випадку забираю адресу в викликаючого обєкта (наприклад Page or Block)

            AddressMG meneger = new AddressMG();
            meneger.numberOfCycles = size;
            meneger.address = 0x90;

            return meneger;

        }
    }
}
